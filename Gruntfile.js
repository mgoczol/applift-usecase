module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        jshint: {
            files: ['Gruntfile.js', 'assets/js/main.js'],
            options: {
                browser: true,
                camelcase: true,
                indent: 4,
                curly: true
            }
        },

        clean: {
            'vendor-js': {
                src: [
                    'assets/js/vendors.js',
                    'assets/js/vendors.min.js'
                ]
            }
        },

        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    'assets/css/style.css': 'assets/css/scss/style.scss',
                }
            }
        },

        concat: {
            options: { separator: ';' },
            'vendor-js': {
                src: [
                    'assets/js/bower_components/jquery/dist/jquery.js',
                    'assets/js/bower_components/owl.carousel/dist/owl.carousel.js'
                ],
                dest: 'assets/js/vendors.js'
            }
        },

        uglify: {
            'vendor-js': {
                files: {
                    'assets/js/vendors.min.js': ['assets/js/vendors.js']
                }
            },
            main: {
                files: {
                    'assets/js/main.min.js': ['assets/js/main.js']
                }
            }
        },

        watch: {
            rebuild: {
                files: [
                    'index.html', 'assets/js/main.js', 'assets/css/**/*.scss'
                ],
                tasks: ['sass-compress', 'jshint'],
                options: {
                    livereload: true,
                    port: 35729
                }
            }
        },

        connect: {
            server: {
                options: {
                    port: 8000,
                    livereload: true
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('clean-vendors', ['clean:vendor-js']);
    grunt.registerTask('vendor-js', ['clean:vendor-js', 'concat:vendor-js', 'uglify:vendor-js']);
    grunt.registerTask('sass-compress', ['sass:dist']);
    grunt.registerTask('minify-custom-js', ['uglify:main']);
    grunt.registerTask('default', ['connect:server', 'watch:rebuild']);
};
