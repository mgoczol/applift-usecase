$(function(){
    'use strict';

    var AppLift = {};

    AppLift.VARS = {
        NAV: $('.jsNav'),
        CAROUSEL: $('.jsCarousel'),
        PARTNERS: $('.jsPartners'),
        LANG_SWITCHER: $('.jsLangSwitcher')
    };

    AppLift.navigation = function($elem) {
        var $trigger = $('.jsNavTrigger'),
            $body = $('body');

        $trigger.on('click', function(e){
            e.preventDefault();
            $body.toggleClass('navigation-active');
        });
    };

    AppLift.carousel = function($elem) {
        $elem.owlCarousel({
            items:1,
            smartSpeed:450,
            nav: true,
            dots: true,
            loop: true
        });
    };

    AppLift.partners = function($elem) {
        var $arrow = $elem.find('.jsArrow'),
            $images = $elem.find('.partner img'),
            $partnerInfo = $elem.find('.partner-info'),
            calculateArrowPosition;


        calculateArrowPosition = function($image) {
            // position + half image width - half arrow width
            return $image.position().left + $image.width() / 2 - 16;
        };

        $images.each(function(i,v) {
            var $image = $(v);

            $image.on('click', function(e) {
                var $info = $image.siblings('.partner-info');
                $arrow.css('left', calculateArrowPosition($image));
                $partnerInfo.addClass('hidden');
                $info.removeClass('hidden');
                // $partnerInfo.fadeOut(200);
                // $info.fadeIn(200);
            });
        });

        // show first partner's quote
        setTimeout(function() {
            var initialPosition = calculateArrowPosition($($images[0]));
            $arrow.css('left', initialPosition);
            $('.partner-info:first').show(0);
        }, 200);
    };

    AppLift.langSwitcher = function($elem) {
        $elem.on('click', function(e) {
            if (!$elem.hasClass('open')) {
                e.preventDefault();
            }
            $elem.toggleClass('open');
        });
    };

    AppLift.init = function() {
        if (this.VARS.NAV.length) {
            this.navigation(this.VARS.NAV);
        }
        if (this.VARS.CAROUSEL.length) {
            this.carousel(this.VARS.CAROUSEL);
        }
        if (this.VARS.PARTNERS.length) {
            this.partners(this.VARS.PARTNERS);   
        }
        if (this.VARS.LANG_SWITCHER.length) {
            this.langSwitcher(this.VARS.LANG_SWITCHER);
        }
    };

    AppLift.init();
});
